Docker Learning

- Buat akun dockerhub
- Buat repository public atau private di website dockerhub
- Nama images yang dibuat harus sesuai seperti berikut


> (Docker ID)/(Repository Name):(tag)

- Melakukan tag pada direktori yang akan di dockerize dan di upload ke dockerhub

> docker tag webapp <ID Docker>/webapp:1.0

- Melakukan push untuk mengupload aplikasi ke dockerhub

> docker push <ID Docker>/webapp:1.0

- Membuat Dockerfile

```
FROM node:current-slim

WORKDIR /usr/src/app
COPY package.json .
RUN npm install

EXPOSE 8080
CMD [ "npm", "start" ]

COPY . .
```

- Melakukan build pada aplikasi yang sudah di dockerize (Pastikan sudah berada di dalam direktori aplikasi yang terdapat file Dockerfile)

> docker build --tag webapp:1.0 .

- Running aplikasi sebagai container

> docker run --publish 8000:8080 --detach --name webapp webapp:1.0

- Untuk menghapus aplikasi yang berjalan di docker

> docker rm --force bb


