Helm Learning

- Generate chart helm dengan menggunakan command berikut
> helm create direktorifile
- Helm akan menggenerate 2 file yaml dalam folder yaitu values dan chart
- Set value image repository pada file value.yaml ke direktori yang digunakan
- Set value service type pada file value.yaml
- Update appversion pada file chart.yaml
- Gunakan command berikut untuk menginstall aplikasi
> helm install namaaplikasi direktoriaplikasi
- Untuk melihat service yang berjalan gunakan command berikut
> kubectl get service