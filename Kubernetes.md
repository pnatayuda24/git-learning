Kubernetes Learning

- Terraform deploy server Kubernetes
- Melihat node Kubernetes yang aktif

> kubectl get nodes

- Membuat namespace baru

> kubectl create namespace kubernetes-belajar

- Melihat namespace yang telah dibuat

> kubectl get namespace

- Membuat yaml file (deployment, pod, service, ingress)
- Mengeksekusi file yaml yang telah dibuat

> kubectl apply -f .

- Melakukan cek apakah service sudah berjalan

```
kubectl get deployment
kubectl get service
kubectl get pods
kubectl get ingress
```

