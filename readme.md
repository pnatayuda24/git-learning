**GIT Learning**

> git init

_Inisialisasi direktori lokal yang akan diupload ke git_


> git remote add origin https://gitlab.com/pnatayuda24/git-learning.git

_membuat koneksi dari direktori lokal ke git_

> git add namafolder/namafile

_memilih folder atau file yang akan diupload ke directory_

> git commit -m “Message Commit”

_pesan dari commit harus berhubungan dengan perubahan yang ada seperti penambahan fitur baru, bug fixing, dll_

> git push origin master

_mengupload folder atau file yang telah di add ke git di branch master_

> git pull origin master

_memperbarui direktori lokal sesuai dengan yang ada di repository_

> git branch test

_membuat branch baru dengan nama ‘test’_

> git checkout test

_pindah ke branch test_

> git merge origin test

_menggabungkan branch test ke branch master_

> git push

_mengembalikan ke semula jika tidak jadi melakukan perubahan yang telah dilakukan_

> git reset –hard

_mengembalikan seperti semula dan kembali ke branch master_
